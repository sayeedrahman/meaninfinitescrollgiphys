(function () {
    'use strict';
    var appModule = angular.module('appModule', ['ngTouch']);

    angular.module('appModule').controller('InfinitScrollController', InfinitScrollController);
    InfinitScrollController.$inject = ['$http', '$swipe', '$window'];
    function InfinitScrollController($http, $swipe, $window) {
        var self = this;
        self.giphys = [];
        //---------------------------------------------------------------
        self.loadMoreRecords = function () {
            //self.Url = "http://api.giphy.com/v1/gifs/search?q=random&api_key=dc6zaTOxFJmzC&limit=100";
            var API_KEY = '9998906-b19a0a5c640d74fa84af93e9a';
            self.Url = "https://pixabay.com/api/?key="+API_KEY+"&q="+encodeURIComponent('red roses');
            $http.get(self.Url).then(function (d) {
                if (d.data) {
                    for (var i = 0; i < d.data.hits.length; i++) {
                        var item = d.data.hits[i];
                        self.giphys.push(item);
                    }
                };
            }, function failure(d) {
                alert('fetching giphys failed!\n' + d.message);
            });
        }
        self.loadMoreRecords();
        //---------------------------------------------------------------
        self.element = angular.element(document).find('#html');
        //---------------------------------------------------------------
        self.onScroll = function () {
            var scrollingElement = self.element;
            var threshold = 200;

            if (scrollingElement[0].scrollHeight - (scrollingElement[0].scrollTop + scrollingElement[0].clientHeight) <= threshold) {
                // Scroll is almost at the bottom. Loading more rows
                self.loadMoreRecords();
            }
        }
        //---------------------------------------------------------------
        angular.element($window).bind("scroll", function (e) {
            self.onScroll();
        });
        //---------------------------------------------------------------
        $swipe.bind(self.element, {
            'start': function (coords) {
                //alert('swipe started!\n,coords.x: ' + coords.x + '\ncoords.y: ' + coords.y);
                self.onScroll();
            },
            'move': function (coords) {
                self.onScroll();
            },
            'end': function (coords) {
                self.onScroll();
            },
            'cancel': function (coords) {
                self.onScroll();
            }
        });
        //---------------------------------------------------------------
    };
})();